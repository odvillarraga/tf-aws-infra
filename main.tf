terraform {
  backend "s3" {
    bucket = "dvillarraga-tf-infra"
    region = "eu-central-1"
    key = "tf-aws-infra"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.6.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

module "my_vpc" {
  source = "./modules/aws-vpc"
}

module "app_security_group" {
  source = "./modules/aws-app-securitygroup"
  vpc_id = module.my_vpc.vpc_id
}

module "app_ecr_repos" {
  source = "./modules/aws-app-ecr-repos"
}