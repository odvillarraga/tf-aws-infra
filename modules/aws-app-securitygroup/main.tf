module "security_group" {
  source = "terraform-aws-modules/security-group/aws"
  version = "4.9.0"
  name        = var.name
  description = "App Security Group"
  vpc_id      = var.vpc_id
  
  egress_rules = ["all-all"]
  ingress_with_self = [{
    rule = "all-all"
  }]
  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["http-80-tcp"]
}