variable "name" {
  description = "SG Name"
  type        = string
  default     = "MyAppSG"
}

variable "vpc_id" {
  description = "VPC ID for the SG"
  type        = string
}