variable "repo_name_snapshots" {
  description = "Repo for dev snapshots"
  type        = string
  default     = "myapp-snapshots"
}

variable "repo_name_releases" {
  description = "Repo for releases"
  type        = string
  default     = "myapp-releases"
}