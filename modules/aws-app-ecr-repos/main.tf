/*
* @ToDo: Limit for Principal section in policy 
*/

## Snapshots-Repo
## ------------------
resource "aws_ecr_repository" "ecr_repo_snapshots" {
  name                 = var.repo_name_snapshots
  image_tag_mutability = "MUTABLE"
}
resource "aws_ecr_repository_policy" "snapshots_access_policy" {
  repository = aws_ecr_repository.ecr_repo_snapshots.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "AllowPushPull",
          "Effect": "Allow",
          "Principal": "*",
          "Action": [
              "ecr:*"
          ]
      }
  ]
}
EOF
}
resource "aws_ecr_lifecycle_policy" "snapshots_lifecycle_policy" {
  repository = aws_ecr_repository.ecr_repo_snapshots.name

  policy = <<EOF
{
  "rules": [
    {
      "rulePriority": 1,
      "description": "Only keep 3 images",
      "selection": {
        "tagStatus": "any",
        "countType": "imageCountMoreThan",
        "countNumber": 3
      },
      "action": { "type": "expire" }
    }
  ]
}
EOF
}

## Releases-Repo
## ------------------

resource "aws_ecr_repository" "ecr_repo_releases" {
  name                 = var.repo_name_releases
  image_tag_mutability = "IMMUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
}
resource "aws_ecr_repository_policy" "ecr_repo_releases_policy" {
  repository = aws_ecr_repository.ecr_repo_releases.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "AllowPushPull",
          "Effect": "Allow",
          "Principal": "*",
          "Action": [
              "ecr:*"
          ]
      }
  ]
}
EOF

}